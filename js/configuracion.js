
  // Import the functions you need from the SDKs you need
  import { initializeApp } 
  from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";

  import { getDatabase,onValue,ref,set,child,get,update,remove } 
  from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";

  import { getStorage, ref as refS, uploadBytes, getDownloadURL }
  from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";

  import { getAuth, signInWithEmailAndPassword, createUserWithEmailAndPassword, signOut } 
  from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";

  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyDCZyuhEal6nWELzO2kIdbSxlXzxO-5RAI",
    authDomain: "webfinal-55324.firebaseapp.com",
    projectId: "webfinal-55324",
    storageBucket: "webfinal-55324.appspot.com",
    messagingSenderId: "829794450032",
    appId: "1:829794450032:web:8f4c04fa8e11af9a8d52b5"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const db = getDatabase();
  const auth = getAuth(app);

  loginbtn.addEventListener('click', function(){
    const loginEmail = document.getElementById("loginemail").value;
    const loginPassword = document.getElementById("loginpassword").value;

   signInWithEmailAndPassword(auth, loginEmail, loginPassword)
  .then((userCredential) => {
    const user = userCredential.user;
    document.getElementById("logindiv").style.display = "none";
    location.href = "admin.html";
    
  })
  .catch((error) => {
    const errorCode = error.code;
    const errorMessage = error.message;
    document.getElementById("logindiv").style.display="none";
    alert("Correo o contraseña incorrecto: " + errorMessage);
    location.href = "error.html";

  });
});



document.getElementById("btnlimpiar").addEventListener('click', function () {
  const loginEmail = document.getElementById("loginemail").value="";
  const loginPassword = document.getElementById("loginpassword").value="";
});



  var btnAgregar = document.getElementById('btnAgregar');
  var btnConsultar = document.getElementById('btnConsultar');
  var btnActualizar = document.getElementById('btnActualizar');
  var btnDeshabilitar = document.getElementById('btnDeshabilitar');
  var btnTodos = document.getElementById('btnTodos');
  var btnLimpiar = document.getElementById('btnLimpiar');
  var lista = document.getElementById('lista');
  var btnVerImagen = document.getElementById('verImagen');
  var archivos = document.getElementById('archivo');

  // Variables inputs
  var codigo = 0;
  var nombre = "";
  var descripcion = "";
  var precio = 0;
  var estatus = 0;
  var url = "";
  var archivo = "";

  function leerInputs(){
    codigo = document.getElementById('codigo').value;
    nombre = document.getElementById('nombre').value;
    descripcion = document.getElementById('descripcion').value;
    precio = document.getElementById('precio').value;
    estatus = document.getElementById('estatus').value;

    archivo = document.getElementById('archivo').value;
    url = document.getElementById('url').value;

  }

  function insertarDatos(){

    leerInputs();

    set(ref(db,'alumnos/' + codigo), {
    nombre:nombre,
    descripcion:descripcion,
    precio:precio,
    estatus:estatus}).then((res)=>{

    alert("Se inserto con exito")

    }).catch((error)=>{

    alert("Surgio un error" + error)
    
    })

  }

  function mostrarProductos(){

    const dbref = ref(db, 'productos');

    onValue(dbref, (snapshot) => {
      lista.innerHTML=""
      snapshot.forEach((childSnapshot) => {
        const childKey = childSnapshot.key;
        const childData = childSnapshot.val();

        lista.innerHTML = "<div class='campo'>" + lista.innerHTML + childKey + " " + childData.nombre
        + " " + childData.descripcion + " " + childData.precio + "" + childData.estatus + "<br> </div>";
        console.log(childKey + ":");
        console.log(childData.nombre)

      });
    }, {
        onlyOnce: true
    });

  }

  function mostrarDatos(){

    leerInputs();

    const dbref = ref(db);

    get(child(dbref,'productos/' + codigo)).then((snapshot)=>{

        if(snapshot.exists()){

            nombre = snapshot.val().nombre;
            descripcion = snapshot.val().descripcion;
            precio = snapshot.val().precio;
            estatus = snapshot.val().estatus;

            escribirInputs();

        }else{

            alert("No se encontro el registro");

        }

    }).catch((error)=>{

    alert("Surgio un error " + error)
        
    })

  }

  function actualizarDatos(){

    leerInputs();
    update(ref(db,'productos/' + codigo),{
      nombre:nombre,
      descripcion:descripcion,
      precio:precio,
    }).then(()=>{
      alert("Se realizo la actualizacion");
      mostrarProductos();
    }).catch(()=>{
      alert("Surgio un error " + error );
    });

  }

  function escribirInputs(){

    document.getElementById('nombre').value = nombre;
    document.getElementById('descripcion').value = descripcion;
    document.getElementById('precio').value = precio;
    document.getElementById('estatus').value = estatus;

  }

  function deshabilitarDatos(){
    leerInputs();
    update(ref(db,'productos/' + codigo),{
      estatus:estatus
    }).then(()=>{
      alert("Se habilito el producto");
      mostrarProductos();
    }).catch(()=>{
      alert("Surgio un error " + error );
    });
  }

  function limpiarDatos(){

    lista.innerHTML="";
    codigo=0;
    nombre="";
    descripcion="";
    precio=0;
    estatus=0;
    escribirInputs();

  }

  function cargarImagen(){
    const file = event.target.files[0]
    const name = event.target.files[0].name;
    document.getElementById('imgNombre').value=name;
    const storage = getStorage();
    const storageRef = refS(storage,'imagenes/' + name);
    uploadBytes(storageRef, file).then((snapshot)=>{
      //Descagar imagen (name)
      alert("Se cargo la imagen");
    });
  }

  btnAgregar.addEventListener('click',insertarDatos);
  btnConsultar.addEventListener('click',mostrarDatos);
  btnActualizar.addEventListener('click',actualizarDatos);
  btnBorrar.addEventListener('click',deshabilitarDatos);
  btnTodos.addEventListener('click',mostrarProductos);
  btnLimpiar.addEventListener('click', limpiarDatos);

  archivos.addEventListener('change',cargarImagen);
  btnVerImagen.addEventListener('click',descargarImagen);

