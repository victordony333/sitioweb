// Import the functions you need from the SDKs you need
import { initializeApp } 
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";

import { getDatabase,onValue,ref,set,child,get,update,remove } 
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";

import { getStorage, ref as refS, uploadBytes, getDownloadURL }
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";

import { getAuth, signInWithEmailAndPassword, createUserWithEmailAndPassword, signOut } 
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDCZyuhEal6nWELzO2kIdbSxlXzxO-5RAI",
  authDomain: "webfinal-55324.firebaseapp.com",
  projectId: "webfinal-55324",
  storageBucket: "webfinal-55324.appspot.com",
  messagingSenderId: "829794450032",
  appId: "1:829794450032:web:8f4c04fa8e11af9a8d52b5"
};

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const db= getDatabase();

  var btnAgregar=document.getElementById('btnAgregar');
  var btnBuscar=document.getElementById('btnBuscar');
  var btnActualizar=document.getElementById('btnActualizar');
  var btnMostrar= document.getElementById('btnMostrar');
  var btnBorrar=document.getElementById('btnBorrar');
  var verImagen= document.getElementById('verImagen');
  var archivos = document.getElementById('archivo');

  var productos=document.getElementById('a');
  var id="";
  var Nombre= "";
  var des="";
  var precio="";
  var estatus="";
  var nombreimg="";
  var url="";
  var archivo="";
  
  function leer(){
    id=document.getElementById('ID').value;
    Nombre=document.getElementById('nombre').value;
    des=document.getElementById('des').value;
    precio=document.getElementById('precio').value;
    estatus=document.getElementById('estatus').value;
    nombreimg= document.getElementById('imgNombre').value;
    url= document.getElementById('URL').value;
    archivo=document.getElementById('archivo').value;
  }

  function escribirinputs(){
    document.getElementById('ID').value=id;
    document.getElementById('nombre').value=Nombre;
    document.getElementById('des').value= des;
    document.getElementById('precio').value=precio;
    document.getElementById('estatus').value=estatus;
    document.getElementById('imgNombre').value=nombreimg;
    document.getElementById('URL').value=url;
  }

  function insertar(){
    leer();
    set(ref(db,"productos/" + id),{
    nombre:Nombre,
    descripcion:des,
    precio:precio,
    estatus:estatus,
    nombreimg: nombreimg,
    url: url

    }).then((docRef)=>{
        alert("Se agrego el registro con exito");
        mostrarProductos();
        limpiar();
    
    }).catch((error)=>{
        alert("Surgio un error", error);
    })

  };

  function mostrarDatos(){
    leer();
    const dbref= ref(db);
  
    get(child(dbref,'productos/' + id)).then((snapshot)=>{
        if(snapshot.exists()){
            Nombre= snapshot.val().nombre;
            des = snapshot.val().descripcion;
            estatus= snapshot.val().estatus;
            precio=snapshot.val().precio;
            nombreimg= snapshot.val().nombreimg;
            url= snapshot.val().url;
            escribirinputs();
        }else{
            alert("No existe la id");
        }
    }).catch((error)=>{
        alert("Surgio un error" + error);
    })
  }

  function actualizar(){
    leer();
    update(ref(db, 'productos/' + id),{
      nombre:Nombre,
      descripcion:des,
      precio:precio,
      estatus:estatus,
      nombreimg: nombreimg,
      url: url
    }).then(()=>{
        alert("Se realizó actualizacion");
        mostrarProductos();
        limpiar();
    }).catch(() => {
        alert("Surgio un error" + error);
        limpiar();
    });
  }

  function deshabilitar(){
    leer();
    update(ref(db, 'productos/' + id),{
      estatus:"No disponible"
    }).then(()=>{
        alert("Se deshabilitó");
        mostrarProductos();
        limpiar();
    }).catch(() => {
        alert("Surgio un error" + error);
    });
  }

  function limpiar(){
    id="";
    Nombre="";
    estatus="";
    precio="";
    des="";
    nombreimg="";
    url="";
    escribirinputs();
  }

  function mostrarProductos(){
    const db = getDatabase();
    const dbRef= ref(db, 'productos');

    onValue(dbRef,(snapshot) => {
        productos.innerHTML="";
        snapshot.forEach((childSnapshot) => {
         
            
            const childData = childSnapshot.val();
            if(childData.estatus=="Disponible"){
              productos.innerHTML= productos.innerHTML +"<div id='pro'>" +   
              "<center>" + 
              "<img src='"+childData.url+"' alt=''>" +
              "<h3>" + " " + childData.nombre + "</h3>" + 
              "<li>" + childData.descripcion + "</li><br>" +
              "<a href=''>$" + childData.precio +"MXN</a>"+ "<br>"+
              "<button id='btn2' >Disponible</button>"+
              "<br>" +
              "</center></div>" ;
            }else if(childData.estatus=="No disponible"){
              productos.innerHTML= productos.innerHTML +"<div id='pro2'>" +
              "<center>" + 
              "<img src='"+childData.url+"' alt=''>" +
              "<h3>" + " " + childData.nombre + "</h3>" + 
              "<li>" + childData.descripcion + "</li><br>" +
              "<a href=''>$" + childData.precio +"MXN</a>"+ "<br>"+
              "<button id='btn2' >Sin disponibilidad</button>"+
              "<br>" +
              "</center></div>" ;
            }
        });
        {
            onlyOnce: true
        }
    });
    
  }
  function cargarImagen(){
    const file= event.target.files[0];
    const name= event.target.files[0].name;
    document.getElementById('imgNombre').value=name;
    const storage= getStorage();
    const storageRef= refS(storage, 'productos/' + name);
  
    uploadBytes(storageRef, file).then((snapshot) => {

  
      alert('se cargo la imagen');
    });
  }
  
  function descargarImagen(){
    archivo= document.getElementById('imgNombre').value;
    // Create a reference to the file we want to download
  const storage = getStorage();
  const starsRef = refS(storage, 'productos/' + archivo);
  
  // Get the download URL
  getDownloadURL(starsRef)
    .then((url) => {
     document.getElementById('URL').value=url;
     document.getElementById('imagen').src=url;
    })
    .catch((error) => {
      // A full list of error codes is available at
      // https://firebase.google.com/docs/storage/web/handle-errors
      switch (error.code) {
        case 'storage/object-not-found':
          console.log("No existe el archivo");
          break;
        case 'storage/unauthorized':
          console.log("No tiene permisos");
          break;
        case 'storage/canceled':
          console.log("No existe conexion con la base de datos")
          break;
  
        // ...
  
        case 'storage/unknown':
          console.log("Ocurrio algo inesperado")
          break;
      }
    });
  }
  
  btnAgregar.addEventListener('click', insertar);
  btnAgregar.addEventListener('change', mostrarProductos);
  btnBuscar.addEventListener('click', mostrarDatos);
  btnActualizar.addEventListener('click', actualizar);
  btnBorrar.addEventListener('click', deshabilitar);
  archivos.addEventListener('change', cargarImagen);
  verImagen.addEventListener('click', descargarImagen); 
