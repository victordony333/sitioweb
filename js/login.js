// Import the functions you need from the SDKs you need
import { initializeApp } 
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";

import { getDatabase,onValue,ref,set,child,get,update,remove } 
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";

import { getStorage, ref as refS, uploadBytes, getDownloadURL }
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";

import { getAuth, signInWithEmailAndPassword, createUserWithEmailAndPassword, signOut } 
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDCZyuhEal6nWELzO2kIdbSxlXzxO-5RAI",
  authDomain: "webfinal-55324.firebaseapp.com",
  projectId: "webfinal-55324",
  storageBucket: "webfinal-55324.appspot.com",
  messagingSenderId: "829794450032",
  appId: "1:829794450032:web:8f4c04fa8e11af9a8d52b5"
};

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const db= getDatabase();

  var btnLogin= document.getElementById('btnLogin');
  var btnLimpiar= document.getElementById('btnLimpiar');

  function login(){
    let email = document.getElementById('usuario').value;
    let contra= document.getElementById('pass').value;

    const auth = getAuth();
    signInWithEmailAndPassword(auth, email, contra)
    .then((userCredential) => {
    // Signed in 
    const user = userCredential.user;
    location.href="/html/admin.html"
    // ...
     })
    .catch((error) => {
    location.href="/html/error.html";
    });
  }

  btnLogin.addEventListener('click', login);